## ReadMe for Republica Scripts

This Repo contains following Scripts:

|: Script Name :| Description | 
| auto_app_installer.sh | Script to install defined Apps |

Author: [Anton Mueller](mailto:scripts@antonmueller.xyz)

