#!/bin/bash -
#===============================================================================
#
#          FILE: auto_app_installer.sh
#
#         USAGE: ./auto_app_installer.sh
#
#   DESCRIPTION: 
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Anton Mueller (am), scripts AT antonmueller DOT xyz 
#       CREATED: 05/23/2024 16:03:40
#===============================================================================

set -o nounset                                  # Treat unset variables as an error
set -eo pipefail

### Variables
# list of apps to install
## eleston-chromium is ungoogled chromium 
## google-chrome would be googles chromium
apps=(vlc microsoft-office libreoffice adobe-acrobat-reader eleston-chromium firefox mas)
store_apps=(Pages Keynote Numbers)

echo "## before installing apps make sure you have all software installed"
softwareupdate -l -a
echo "$$ make sure you install and download all updates in a new tab"

if ! xcode-select -p &> /dev/null; then
    echo "xcode-select is not installed. Starting installation..."
    # Install xcode-select
    sudo xcode-select --install
    echo "Please follow the on-screen instructions to complete the installation."
fi
#check for arm64
echo "## install rosetta2m if this is an arm64"
if [[ $(uname -m) == 'arm64' ]]; 
	then sudo softwareupdate --install-rosetta --agree-to-license 
fi

# install homebrew
echo "## check for homebrew, if not found install it"
which brew || /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

echo "## install apps"
for app in "${apps[@]}"; do
	brew install $app
	echo "## $app installed"
done

echo "## install store apps"
for app in "${store_apps[@]}"; do	
	mas install $(mas search $app | awk '{print $1}' | head -n 1)
	echo "## $app installed"
done
